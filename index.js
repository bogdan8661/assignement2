
const FIRST_NAME = "Bogdan";
const LAST_NAME = "Tutunea";
const GRUPA = "1085";

function initCaching() 
{
    
    var cache=new Object();

    cache.getCache=function()
    {
        return cache;
    }

    cache.pageAccessCounter=function(s)
    {
    
        if(cache.hasOwnProperty(s))
        {
            cache[s]+=1;
        }
        else cache[s]=1;
        if (arguments[0]==null) cache[0]+=1;
        
        return cache;
    }

    return cache;       
}



module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

